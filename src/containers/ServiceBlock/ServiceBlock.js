import React, {Component, Fragment} from 'react';
import './ServiceBlock.css'
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";

class ServiceBlock extends Component {
    render() {
        return (
            <Fragment>
                <header>
                    <Header/>
                </header>
                <div className="service-block">
                    <div className="container">
                        <h2>Our Services</h2>
                        <span>Lorem Ipsum is simply dummy Business industry.</span>
                        <div className="services">
                            <div className="service service-1">
                                <a href="#">
                                    <i className="far fa-clock fa-4x" />
                                    <h4>Font-Awesome Icons</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting ind ustry. Lorem Ipsum
                                        has been the industry's standard dummy text ever since the 1500s, when an unknown
                                        printer.</p>
                                </a>
                            </div>
                            <div className="service service-2">
                                <a href="#">
                                    <i className="fas fa-cog fa-4x" />
                                    <h4>Fully Responsive Design</h4>
                                    <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their
                                        default model text, and a search for 'lorem ipsum' will uncover many web sites still in
                                        their infancy</p>
                                </a>
                            </div>
                            <div className="service service-3">
                                <a href="#">
                                    <i className="fas fa-graduation-cap fa-4x" />
                                    <h4>Working Contact form</h4>
                                    <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their
                                        default model text, and a search for 'lorem ipsum' will un cover many web sites still in
                                        their infancy</p>
                                </a>
                            </div>
                            <div className="service service-4">
                                <a href="#">
                                    <i className="fas fa-rocket fa-4x" />
                                    <h4>Retina Ready icons here</h4>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have
                                        suffered alteration in some form, by injected humour or randomised words which don't
                                        look even slightly believable.</p>
                                </a>
                            </div>
                            <div className="service service-5">
                                <a href="#">
                                    <i className="fab fa-app-store fa-4x" />
                                    <h4>Friendly Code</h4>
                                    <p>Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form,
                                        accompanied by English versions from the 1914 translation by H. Rackham</p>
                                </a>
                            </div>
                            <div className="service service-6">
                                <a href="#">
                                    <i className="fas fa-dollar-sign fa-4x" />
                                    <h4>Flat trendy landing page Design</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                        has been the industry's standard dummy text ever since the 1500s, when an unknown
                                        printer.</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <footer>
                    <Footer/>
                </footer>
            </Fragment>
        );
    }
}

export default ServiceBlock;