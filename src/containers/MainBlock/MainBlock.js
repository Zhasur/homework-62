import React, {Component, Fragment} from 'react';
import './MainBlock.css'
import Laptop from '../../assets/laptop.png'
import Footer from "../../components/Footer/Footer";
import Header from "../../components/Header/Header";

class MainBlock extends Component {
    render() {
        return (
            <Fragment>
                <header>
                    <Header/>
                </header>
                <div className='main-block'>
                    <div className='container'>
                        <div className="title">
                            <h1>Boost up your Local business</h1>
                            <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum typesetting</span>
                        </div>
                        <img className="main-pic" src={Laptop} alt="Laptop" width="648" height="343" />
                    </div>
                </div>
                <div className="submain" />
                <footer>
                    <Footer/>
                </footer>
            </Fragment>
        );
    }
}

export default MainBlock;