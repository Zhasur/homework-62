import React, { Component } from 'react';
import './App.css';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import MainBlock from "./MainBlock/MainBlock";
import ServiceBlock from "./ServiceBlock/ServiceBlock";
import Clients from './Clients/Clients'
import Login from './Login/Login'

class App extends Component {
  render() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={MainBlock} />
                <Route path="/service-block" component={ServiceBlock} />
                <Route path="/clients-block" component={Clients} />
                <Route path="/log-in" component={Login} />
            </Switch>
        </BrowserRouter>
    );
  }
}

export default App;
