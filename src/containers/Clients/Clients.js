import './Clients.css'
import React, {Component, Fragment} from 'react';
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";

class Clients extends Component {
    render() {
        return (
            <Fragment>
                <header>
                    <Header/>
                </header>
                <div className="block-clients">
                    <div className="container">
                        <h2>Happy Clients</h2>
                        <span className="subtitle">We are explain who is using our business solutions</span>
                        <div className="clients">
                            <div className="client client-1">
                                <p><i className="fas fa-quote-left"></i>Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the
                                    1500s, when an unknown printer took a galley of type and scrambled it to make.
                                    <i className="fas fa-quote-right"></i>
                                </p>
                                <a href="#">Darwin Michle,</a>
                                <span>Project manager</span>
                            </div>
                            <div className="client client-2">
                                <p><i className="fas fa-quote-left"></i>Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry. Lorem Ipsum has been the industrystandard dummy text ever since the
                                    1500s, wen an unknown printer took a galley of type and scrambled to make a type specimen
                                    book.<i className="fas fa-quote-right"></i>
                                </p>
                                <a href="#">Madam Elisabath,</a>
                                <span>Creative Director</span>
                            </div>
                            <div className="client client-3">
                                <p><i className="fas fa-quote-left"></i>Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the
                                    1500s, when an unknown printer took a galley of type and scrambled it to make.
                                    <i className="fas fa-quote-right"></i>
                                </p>
                                <a href="#">Clips arter,</a>
                                <span>Lipsum directer</span>
                            </div>
                            <div className="client client-4">
                                <p><i className="fas fa-quote-left"></i>Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry. Lorem Ipsum has been the industrystandard dummy text ever since the
                                    1500s, wen an unknown printer took a galley of type and scrambled to make a type specimen
                                    book.<i className="fas fa-quote-right"></i>
                                </p>
                                <a href="#">zam cristafr,</a>
                                <span>manager</span>
                            </div>
                        </div>
                    </div>
                    <footer>
                        <Footer/>
                    </footer>
                </div>
            </Fragment>
        );
    }
}

export default Clients;