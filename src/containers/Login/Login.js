import React, {Component, Fragment} from 'react';
import Avatar from '../../assets/avatar.png'
import './Login.css'
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";

class Login extends Component {
    render() {
        return (
            <Fragment>
                <header>
                    <Header/>
                </header>
            <div className="login-block">
                <div className="login-box">
                    <img src={Avatar} className="avatar" />
                    <h1> Login Here</h1>
                    <form>
                        <p>Username</p>
                        <input type="text" name="username" placeholder="Enter Username" />
                        <p>Password</p>
                        <input type="password" name="password" placeholder="Enter Password" />
                        <input type="submit" name="submit" value="Login" />
                    </form>
                </div>
            </div>
                <footer className="login-footer">
                    <Footer/>
                </footer>
            </Fragment>

        );
    }
}

export default Login;