import React, {Component} from 'react';
import './Header.css'
import Logo from '../../assets/logo.png'
import { NavLink } from 'react-router-dom'


class Header extends Component {

    render() {
        return (
            <div className="container">
                <NavLink to="/" className="logo"><img src={Logo} alt=""/></NavLink>
                <ul className="main-nav">
                    <li>
                        <NavLink to="/" exact>Home</NavLink>
                    </li>
                    <li>
                        <NavLink to="/service-block">Service</NavLink>
                    </li>
                    <li>
                        <NavLink to="/clients-block">Happy Clients</NavLink>
                    </li>
                    <li>
                        <NavLink to="/log-in">Log in</NavLink>
                    </li>
                </ul>
            </div>
        );
    }
}

export default Header;