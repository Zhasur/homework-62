import React from 'react';
import './Footer.css'

const Footer = () => {
    return (
        <div className="container">
            <span className="footer-span">All contents © copyright 2014 Business Theme. All rights reserved Designed by : akhilwebfolio</span>
        </div>
    );
};

export default Footer;